package vulnapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vulnapp.entity.Location;

public interface LocationRepository extends JpaRepository<Location, Long> {
    Location findByCode(String code);

    Location findByCoordinates(String coordinates);

}