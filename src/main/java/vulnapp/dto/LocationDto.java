package vulnapp.dto;

import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LocationDto {
    private Long id;
    @NotEmpty(message = "Locations should not be empty")
    private String coordinates;
    @NotEmpty(message = "Code should not be empty")
    private String code;
}
