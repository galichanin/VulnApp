package vulnapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.web.servlet.MockMvc;
import vulnapp.controller.LocationController;


@WebMvcTest(LocationController.class)
class VulnApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@TestConfiguration
	static class TestConfig {


	}
}