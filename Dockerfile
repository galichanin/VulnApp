FROM bellsoft/liberica-openjdk-alpine:17
RUN apk --no-cache add openssl
ARG JAR_FILE=target/VulnApp-0.0.1-SNAPSHOT.jar
WORKDIR /opt/app
COPY ${JAR_FILE} app.jar
RUN adduser --system --no-create-home nonroot
USER nonroot
HEALTHCHECK CMD curl --fail http://localhost:8080/ || exit 1
ENTRYPOINT ["java","-jar","app.jar"]